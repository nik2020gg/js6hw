function createNewUser() {
  var newUser = {};

  var firstName = prompt("Введіть імя:");
  var lastName = prompt("Введіть прізвище:");
  var birthday = prompt("Введіть дату нарождення (dd.mm.yyyy):");

  newUser.firstName = firstName;
  newUser.lastName = lastName;
  newUser.birthday = birthday;

  newUser.getAge = function () {
    var today = new Date();
    var birthDate = new Date(this.birthday);
    var age = today.getFullYear() - birthDate.getFullYear();
    var monthDiff = today.getMonth() - birthDate.getMonth();
    if (
      monthDiff < 0 ||
      (monthDiff === 0 && today.getDate() < birthDate.getDate())
    ) {
      age--;
    }
    return age;
  };

  newUser.getPassword = function () {
    var firstLetter = this.firstName.charAt(0).toUpperCase();
    var lowercaseLastName = this.lastName.toLowerCase();
    var birthYear = this.birthday.split(".")[2];
    return firstLetter + lowercaseLastName + birthYear;
  };

  return newUser;
}

var user = createNewUser();

console.log(user);
console.log("Вік користувача:", user.getAge());
console.log("Пароль користувача:", user.getPassword());

// це механізм, який дозволяє вставляти спеціальні символи або послідовності символів у рядки, що використовуються у програмах,
// function,var functionName, ці способи дають можливіть створювати функції
// механізм, при якому змінні та оголошення функції піднімаються вгору по своїй області видимості перед виконанням коду
